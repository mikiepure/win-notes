@echo off
cd /d %~dp0
setlocal EnableDelayedExpansion

REM このファイル名と同じ名前の "イーサネットアダプター" の IP 設定を変更する
set INTERFACE_NAME="%~n0"

openfiles > nul 2>&1
if %ERRORLEVEL% neq 0 (
  REM run as user mode
  powershell -Command Start-Process -FilePath """%~nx0""" -Verb Runas
) else (
  REM run as admin mode
  echo インターフェース !INTERFACE_NAME! の IP 設定を変更します。
  echo ###########################################################################
  set /p DHCP="IP 設定に DHCP を使用しますか[Y/n]？: "
  echo ###########################################################################

  if /i not "!DHCP!" == "n" (
    echo インターフェース !INTERFACE_NAME! を DHCP に設定します。
    netsh interface ip set add !INTERFACE_NAME! dhcp
    netsh interface ip set dns !INTERFACE_NAME! dhcp
  ) else (
    echo インターフェース !INTERFACE_NAME! を Static に設定します。
    echo ###########################################################################
    set /p IP_ADDR="IP Address: "
    set /p SUBMASK="SubnetMask: "
    echo ###########################################################################
    netsh interface ip set add !INTERFACE_NAME! static !IP_ADDR! !SUBMASK!
  )

  echo インターフェース !INTERFACE_NAME! に設定を反映します。
  netsh interface set interface !INTERFACE_NAME! disable
  netsh interface set interface !INTERFACE_NAME! enable
  echo インターフェース !INTERFACE_NAME! の設定が完了しました。
)
